import { useState, useCallback } from 'react'

export const useInputControl = (
	inittext: string,
): [string, React.ChangeEventHandler<HTMLInputElement>, () => void] => {
	const [text, settext] = useState(inittext)
	const onInput = useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			if (e.target instanceof HTMLInputElement) {
				settext(e.target.value)
			}
		},
		[settext],
	)
	const resetText = useCallback(() => {
		settext('')
	}, [settext])
	return [text, onInput, resetText]
}

export const useOnEnterKeyDown = (fn: () => void, p: any[]) => {
	return useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.nativeEvent.isComposing) return
		if ('Enter' === e.key) {
			fn()
		}
	}, p)
}
