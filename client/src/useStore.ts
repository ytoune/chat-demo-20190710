import { useState, useEffect } from 'react'
import { create } from './store'
import { Message } from './store/messages'

type Store = {
	messages: Message[]
	loginedUserId?: number
}

const { store, actions } = create('ws://localhost:8080')

export const useStore = () => {
	const [v, set] = useState<Store>({ messages: [] })
	useEffect(() => {
		const s = store.subscribe(s => set(s))
		return () => {
			s.unsubscribe()
		}
	}, [])
	const isLogined = 'number' === typeof v.loginedUserId
	const { signin, sendMessage } = actions
	return { ...v, isLogined, signin, sendMessage }
}
