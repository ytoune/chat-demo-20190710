import { RequestHandler } from 'express'

import { repos, Token, isSpeaker } from '../db'
import { parseFromISO } from '../utils/parseFromISO'
import { randomAsciiString } from '../utils/randomAsciiString'

// type HttpPostTokensBody = {
// 	expiredAt: string // ISO 8601 date string
// 	speaker: Speaker
// 	roomName: string
// }
// type HttpPostTokensResponseBody = {
// 	token: string // Token.code
// }

export const createToken: RequestHandler = (req, res, next) => {
	const { body } = req
	const sendError = (message: string) => res.status(400).json({ message })
	if (!body) return sendError('invalid body.')

	const { expiredAt: expiredAtStr, speaker, roomName } = body
	if (!isSpeaker(speaker)) return sendError('speaker required.')
	if ('string' !== typeof roomName) return sendError('roomName required.')
	const expiredAt = parseFromISO(expiredAtStr)
	if (!expiredAt) return sendError('expiredAt required.')
	const code = randomAsciiString(128)
	const token: Token = { code, roomName, speaker, expiredAt }
	return repos.tokens
		.save(token)
		.then(ok => {
			if (!ok) return sendError('failed to save token.')
			return res.status(200).json({ token: code })
		})
		.catch(next)
}
