import { Entity, PrimaryColumn, Column } from 'typeorm'
import { Speaker } from './Speaker'

@Entity('tokens')
export class Token {
	@PrimaryColumn({ type: 'varchar', length: 128 })
	code!: string

	@Column({ type: 'json' })
	speaker!: Speaker

	@Column({ type: 'varchar', length: 180 })
	roomName!: string

	@Column({ type: 'datetime' })
	expiredAt!: Date
}
