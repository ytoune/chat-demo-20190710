import { Repo, RepoQuery, Observer } from '../interfaces'

type Where<K> = [K, '=' | '>' | '<', any]

class Query<T> implements RepoQuery<T> {
	protected _where: Where<keyof T>[] = []
	protected _list: T[] = []
	where<K extends keyof T>(key: K, op: '=' | '>' | '<', value: T[K]): this {
		this._where.push([key, op, value])
		return this
	}
	async get(): Promise<T[]> {
		const w = this._where
		return this._list.filter(u => {
			return w.every(([k, o, v]) => {
				switch (o) {
					case '=':
						return u[k] === v
					case '>':
						return u[k] > v
					case '<':
						return u[k] < v
				}
			})
		})
	}
	async first(): Promise<T | null> {
		return (await this.get())[0] || null
	}
}

export class Message {
	id?: number
	speaker?: { name: string }
	text?: string
}
export class Messages implements Repo<Message, number> {
	private _watchers: Observer<Message>[] = []
	select() {
		return new MessageSelect()
	}
	async find(value: number) {
		if (!value) return null
		return await this.select()
			.where('id', '=', value)
			.first()
	}
	async destory(value: number) {
		if (!value) return true
		const i = messagelist.findIndex(m => value === m.id)
		if (~i) messagelist.splice(i, 1)
		return true
	}
	async save(item: Message) {
		if (item.id) {
			const t = await this.find(item.id)
			if (!t) return false
			for (const [i, v] of Object.entries(item)) {
				//@ts-ignore
				t[i] = v
			}
			for (const w of this._watchers) w.next(t)
		} else {
			item.id = message_id++
			messagelist.push(item)
			for (const w of this._watchers) w.next(item)
		}
		return true
	}
	watch(observer: Partial<Observer<Message>>): () => void {
		const observerc: Observer<Message> = {
			next() {},
			error() {},
			complete() {},
			...observer,
		}
		this._watchers.push(observerc)
		for (const u of messagelist) observerc.next(u)
		return () => {
			const i = this._watchers.indexOf(observerc)
			if (~i) this._watchers.splice(i, 1)
		}
	}
	push(text: string) {
		const item = { id: message_id++, text }
		for (const w of this._watchers) w.next(item)
	}
}
let message_id = 1
const messagelist: Message[] = []
class MessageSelect extends Query<Message> {
	constructor() {
		super()
		this._list = messagelist
	}
}

export const repos = {
	messages: new Messages(),
}
