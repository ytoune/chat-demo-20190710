import React from 'react'
import { useStore } from '../useStore'
import { useInputControl, useOnEnterKeyDown } from './libs'
import { Paper, TextField } from '@material-ui/core'

export const LoginForm = () => {
	const { signin } = useStore()
	const [name, onChange, resetText] = useInputControl('')
	const onKeyDown = useOnEnterKeyDown(() => {
		signin(name)
		resetText()
	}, [name, resetText])
	return (
		<Paper>
			<TextField
				value={name}
				onChange={onChange}
				onKeyDown={onKeyDown}
				fullWidth
				placeholder="ユーザ名"
			/>
		</Paper>
	)
}
