import { RequestHandler } from 'express'

const { CHAT_ADMIN_TOKEN_KEY = 'x-admin-token', CHAT_ADMIN_TOKEN } = process.env

export const checkHeader: RequestHandler = (req, res, next) => {
	if (!CHAT_ADMIN_TOKEN) return next()
	const token = req.headers[CHAT_ADMIN_TOKEN_KEY]
	if ('string' === typeof token) {
		if (CHAT_ADMIN_TOKEN === token) return next()
	} else if (token && 1 === token.length) {
		if (CHAT_ADMIN_TOKEN === token.join()) return next()
	}
	return res.status(401).json({ message: 'unauthorized' })
}
