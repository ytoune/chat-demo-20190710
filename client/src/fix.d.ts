interface KeyboardEvent {
	isComposing?: boolean
}

declare global {
	interface KeyboardEvent {
		isComposing?: boolean
	}
}
