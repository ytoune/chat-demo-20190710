import { Repo, RepoQuery, Observer, Keytype } from '../interfaces'
import { conn } from './conn'
import { Message } from './Message'

import {
	Equal,
	MoreThan,
	LessThan,
	EntitySchema,
	FindConditions,
	DeepPartial,
	FindOperator,
} from 'typeorm'
import { Token } from './Token'

type FindOp = <T>(value: T) => FindOperator<T>

const createRepo = <T, K extends keyof T>(
	Model: (Function & (new (...a: any) => T)) | EntitySchema<T>,
	keyname: K,
) => {
	type P = T[K] extends Keytype ? Exclude<T[K], undefined> : never
	class Query implements RepoQuery<T> {
		private _q: FindConditions<T> = {}
		where<K extends keyof T>(key: K, op: '=' | '>' | '<', value: T[K]): this {
			const mk: FindOp = { '=': Equal, '>': MoreThan, '<': LessThan }[op]
			;(this._q as any)[key] = mk(value)
			return this
		}
		async get(): Promise<T[]> {
			return await (await conn).getRepository(Model).find({ where: this._q })
		}
		async first(): Promise<T | null> {
			const r = await (await conn)
				.getRepository(Model)
				.findOne({ where: this._q })
			return r || null
		}
	}
	const select = (): RepoQuery<T> => new Query()
	const find = (value: P): Promise<T | null> => {
		if (!value) return Promise.resolve(null)
		return select()
			.where(keyname, '=', value)
			.first()
	}
	const save = async (item: Partial<T>): Promise<boolean> => {
		const r = await (await conn)
			.getRepository(Model)
			.save(item as DeepPartial<T>)
		for (const w of watchers) w.next(r)
		return !!r
	}
	const destory = async (value: P): Promise<boolean> => {
		if (!value) return true
		//@ts-ignore
		const where: FindConditions<T> = { [keyname]: Equal(value) }
		const r = await (await conn).getRepository(Model).delete(where)
		return !!r.affected
	}
	const watchers: Observer<T>[] = []
	const watch = (observer: Partial<Observer<T>>): (() => void) => {
		const observerc: Observer<T> = {
			next() {},
			error() {},
			complete() {},
			...observer,
		}
		watchers.push(observerc)
		select()
			.get()
			.then(messagelist => {
				for (const u of messagelist) observerc.next(u)
			})
		return () => {
			const i = watchers.indexOf(observerc)
			watchers.splice(i, 1)
		}
	}
	const repo: Repo<T, P> = { select, find, destory, save, watch }
	return repo
}

export const repos = {
	messages: createRepo(Message, 'id'),
	tokens: createRepo(Token, 'code'),
}
