import { createConnection } from 'typeorm'
import { Message } from './Message'
import { Token } from './Token'

const {
	MYSQL_DATABASE,
	MYSQL_USERNAME,
	MYSQL_PASSWORD,
	MYSQL_HOST,
	DEBUG_LOGGING,
} = process.env

const getConnection = () => {
	if (!MYSQL_DATABASE) throw new Error('env.MYSQL_DATABASE must be defined')
	if (!MYSQL_USERNAME) throw new Error('env.MYSQL_USERNAME must be defined')
	if (!MYSQL_PASSWORD) throw new Error('env.MYSQL_PASSWORD must be defined')
	if (!MYSQL_HOST) throw new Error('env.MYSQL_HOST must be defined')

	return createConnection({
		type: 'mysql',
		port: 3306,
		host: MYSQL_HOST,
		username: MYSQL_USERNAME,
		password: MYSQL_PASSWORD,
		database: MYSQL_DATABASE,
		entities: [Message, Token],
		synchronize: true,
		logging: !!DEBUG_LOGGING,
	})
}

export const conn = getConnection()
