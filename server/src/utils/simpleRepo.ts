import { Subject } from 'rxjs'
import { Repo, RepoQuery, Observer } from '../interfaces'

export const simpleRepo = <T>() => {
	const sub = new Subject<T>()
	const select = (): RepoQuery<T> => {
		const where = () => qry
		const get = async () => []
		const first = async () => null
		const qry = { where, get, first }
		return qry
	}
	const find = async () => null
	const destory = async () => true
	const save = async (value: T) => {
		sub.next(value)
		return true
	}
	const watch = (observer: Partial<Observer<T>>) => {
		const observerc: Observer<T> = {
			next() {},
			error() {},
			complete() {},
			...observer,
		}
		const s = sub.subscribe(observerc)
		return () => s.unsubscribe()
	}
	const repo: Repo<T, undefined> = { select, find, destory, save, watch }
	return repo
}
