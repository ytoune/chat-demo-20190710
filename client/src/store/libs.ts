import { parse } from 'date-fns'
import { Observable as Obsrv, isObservable, of, combineLatest } from 'rxjs'

export const parseFromISO = (input: unknown) => {
	if ('string' !== typeof input) return
	try {
		return parse(input)
	} catch {}
	return
}

export const isRecord = (o: unknown): o is Record<keyof any, unknown> =>
	'object' === typeof o && !!o

export const isNullOrUndef = (o: unknown): o is null | undefined =>
	null === o || undefined === o

export const combinePromises = <
	<T extends {}>(
		obj: T,
	) => Promise<{ [K in keyof T]: T[K] extends Promise<infer U> ? U : T[K] }>
>((r: Record<keyof any, Promise<any>>) => {
	const k = Object.keys(r)
	return Promise.all(k.map(k => r[k])).then(v =>
		v.reduce((d, v, i) => ({ ...d, [k[i]]: v }), {}),
	)
})

export const combineObservables = <
	<T extends {}>(
		obj: T,
	) => Obsrv<{ [K in keyof T]: T[K] extends Obsrv<infer U> ? U : T[K] }>
>((r: any) => {
	const k = Object.keys(r)
	return combineLatest(
		...k.map(k => r[k]).map(v => (isObservable(v) ? v : of(v))),
		(...v) => v.reduce((d, v, i) => ({ ...d, [k[i]]: v }), {}),
	)
})
