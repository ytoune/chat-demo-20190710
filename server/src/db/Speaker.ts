export type Speaker = {
	name: string
	id?: any
	avatar?: string // 画像の URI
}

const isRecord = (o: unknown): o is Record<keyof any, unknown> =>
	'object' === typeof o && !!o

export const isSpeaker = (speaker: unknown): speaker is Speaker =>
	isRecord(speaker) &&
	'string' === typeof speaker.name &&
	(null == speaker.avatar || 'string' === typeof speaker.avatar)
