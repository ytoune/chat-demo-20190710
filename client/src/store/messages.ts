import { ReplaySubject } from 'rxjs'
import { scan, startWith, map } from 'rxjs/operators'
import { isRecord, isNullOrUndef, parseFromISO } from './libs'
import { compareAsc } from 'date-fns'

export type Message = {
	id: number
	text: string
	roomName: string
	speaker: Speaker | null
	replyTo: number | null
	createdAt: Date
	updatedAt: Date
}

export type Speaker = {
	name: string
	id?: any
	avatar?: string
}
export const isSpeaker = (speaker: unknown): speaker is Speaker =>
	isRecord(speaker) &&
	'string' === typeof speaker.name &&
	(isNullOrUndef(speaker.avatar) || 'string' === typeof speaker.avatar)

type MessageMap = Map<Message['id'], [Message | null, Date]>

export const asMessage = (o: unknown): Message | null => {
	if (
		isRecord(o) &&
		'number' === typeof o.id &&
		'string' === typeof o.text &&
		'string' === typeof o.roomName &&
		(isSpeaker(o.speaker) || isNullOrUndef(o.speaker)) &&
		('number' === typeof o.replyTo || isNullOrUndef(o.replyTo))
	) {
		const { id, text, roomName, speaker = null, replyTo = null } = o
		const createdAt = parseFromISO(o.createdAt)
		const updatedAt = parseFromISO(o.updatedAt)
		if (createdAt && updatedAt)
			return { id, text, roomName, speaker, replyTo, createdAt, updatedAt }
	}
	return null
}

const reducer = (list: MessageMap, mes: Message): MessageMap => {
	const c = list.get(mes.id)
	if (c && compareAsc(mes.createdAt, c[1]) < 0) {
		list.set(mes.id, [mes, c[1]])
	} else {
		list.set(mes.id, [mes, mes.createdAt])
	}
	if (mes.replyTo) {
		const p = list.get(mes.replyTo)
		if (p) {
			if (compareAsc(p[1], mes.createdAt) < 0)
				list.set(mes.replyTo, [p[0], mes.createdAt])
		} else {
			list.set(mes.replyTo, [null, mes.createdAt])
		}
	}
	return list
}

type Pair = [Message, Date]
const orderByCreatedAt = (q: Pair, w: Pair) =>
	compareAsc(q[1], w[1]) || q[0].id - w[0].id
const toList = (map: MessageMap): Message[] => {
	const list: Pair[] = []
	for (const [mes, date] of map.values()) {
		if (mes) list.push([mes, date])
	}
	return list.sort(orderByCreatedAt).map(u => u[0])
}

export const create = () => {
	const messages = new ReplaySubject<Message>(1)
	const message$ = messages.pipe(
		scan(reducer, new Map()),
		map(toList),
		startWith<Message[], Message[]>([]),
	)

	const pushMessage = (mes: unknown) => {
		const m = asMessage(mes)
		if (m) messages.next(m)
	}

	return { message$, pushMessage }
}
