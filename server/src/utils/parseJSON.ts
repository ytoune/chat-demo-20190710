type ParseJSON = {
	(...a: Parameters<typeof JSON.parse>):
		| undefined
		| null
		| Record<keyof any, unknown>
}

/**
 * return undefined on an error happened
 */
export const parseJSON: ParseJSON = (...a) => {
	try {
		return JSON.parse(...a)
	} catch {}
}
