import { ReplaySubject, of, Observable, Subject } from 'rxjs'
import { startWith, shareReplay, catchError, scan } from 'rxjs/operators'
import { webSocket } from 'rxjs/webSocket'
import { isRecord, isNullOrUndef, combineObservables } from './libs'
import { create as createMess, Message, Speaker, isSpeaker } from './messages'

export { Message, Speaker }

export type Store = {
	messages: Message[]
	loginedUserId?: number
	errors: string[]
	fatalErrors: any[]
}

export type Actions = ReturnType<typeof create>['actions']

export type CustomData = {
	speaker: Speaker | null
	payload: any
}

export const create = (uri: string) => {
	const { message$, pushMessage } = createMess()

	const loginedUserIds = new ReplaySubject<number | undefined>(1)
	const loginedUserId$ = loginedUserIds.pipe(
		startWith<number | undefined, undefined>(undefined),
	)

	const errors = new ReplaySubject<string | null>(1)
	const error$ = errors.pipe(
		scan((l: string[], m) => (m ? [...l, m] : []), []),
		startWith(<string[]>[]),
	)

	const fatalErrors = new ReplaySubject<any>(1)
	const fatalError$ = fatalErrors.pipe(
		scan((l: any[], m) => [...l, m], []),
		startWith(<any[]>[]),
	)

	const customDatas = new Subject<CustomData>()

	const store: Observable<Store> = combineObservables({
		messages: message$,
		loginedUserId: loginedUserId$,
		errors: error$,
		fatalErrors: fatalError$,
	}).pipe(shareReplay(1))

	const subscription = store.subscribe()

	const subject = webSocket(uri)

	const signin = (token: string) => {
		subject.next({ type: 'signin', token })
	}
	const close = () => {
		errors.complete()
		loginedUserIds.next()
		loginedUserIds.complete()
		subject.complete()
		subscription.unsubscribe()
	}
	const sendMessage = (text: string) => {
		subject.next({ type: 'speak', text })
	}

	subject
		.pipe(
			catchError(x => {
				if (x instanceof CloseEvent) return of({ type: 'connection-closed' })
				throw x
			}),
		)
		.subscribe({
			next: mes => {
				if (isRecord(mes))
					switch (mes.type) {
						case 'message':
							pushMessage(mes.message)
							break
						case 'singin-success':
							errors.next(null)
							loginedUserIds.next(1)
							break
						case 'singin-failed':
							if ('string' === typeof mes.reason) errors.next(mes.reason)
							break
						case 'connection-closed':
							loginedUserIds.next()
							errors.next(mes.type)
							break
						case 'custom':
							if (isSpeaker(mes.speaker) || isNullOrUndef(mes.speaker)) {
								const { speaker = null, payload } = mes
								customDatas.next({ speaker, payload })
							}
							break
					}
			},
			error: x => {
				console.error(x)
			},
		})

	return { store, actions: { signin, close, sendMessage }, customDatas }
}
