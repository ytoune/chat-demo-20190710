import * as http from 'http'
import * as express from 'express'
import * as WebSocket from 'ws'
import { json as jsonParser } from 'body-parser'
import { onConnect } from './core/onConnect'
import { createToken } from './core/createToken'
import { removeToken } from './core/removeToken'
import { handleError, handleErrorWs } from './core/handleError'
import { checkHeader } from './core/checkHeader'

const app = express()
const server = http.createServer(app)

const router = express.Router()
router.use(checkHeader)
router.use(jsonParser({ type: '*/*' }))
router.post('/tokens', createToken)
router.delete('/tokens/:code', removeToken)

app.use(router)
app.use(handleError)

const wss = new WebSocket.Server({ server })
wss.on('connection', onConnect)
wss.on('error', handleErrorWs)

export { server as app }
