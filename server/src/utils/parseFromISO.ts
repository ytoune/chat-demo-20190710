import { parse } from 'date-fns'

export const parseFromISO = (input: unknown) => {
	if ('string' !== typeof input) return
	try {
		return parse(input)
	} catch {}
	return
}
