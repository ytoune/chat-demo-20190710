#### チャット機能の実装のテスト用

php + mysql なサービス内に埋め込むための少数のユーザ向けのチャットサービス。

シングルプロセスで動かさないとうまく動かない。  
将来ユーザが多くなったら firestore や redis や kinesis あたりを使った実装を考えることになりそう。  


---

#### 試す

```sh
docker-compose up -d
(cd server && yarn && yarn start)&
(cd client && yarn && yarn start)&
```

`http://localhost:1234` を開く

---

#### メッセージを投げあうまでの流れ

```mermaid
sequenceDiagram

  participant main as メインサービス
  participant chat as このシステム
  participant client as クライアント

  main->>chat: トークンの作成要請
  chat->>main: トークン

  main->>client: 認証済ユーザにトークンを渡す
  client->>chat: トークンでログイン
  chat->>client: ログイン成功通知
```

---

#### エンドポイント

```ts
// HTTP POST /tokens
type HttpPostTokensBody = {
  expiredAt: string // ISO 8601 date string
  speaker: Speaker
  roomName: string
}
type HttpPostTokensResponseBody = {
  token: string // Token.code
}

// HTTP DELETE /tokens/:code
type HttpDeleteTokenBody = undefined
type HttpDeleteTokenResponseBody = {
  ok: true
}
```

```ts
// WS /*
type WsSendMessage =
  | SignIn
  | PostMessage
  | CustomMessage

type SignIn = {
  type: 'signin'
  token: string
}
type PostMessage = {
  type: 'speak'
  text: string
}
type CustomMessage = {
  type: 'custom'
  payload: any
}

type WsReceiptedMessage =
  | SuccessToSingIn
  | FailedToSingIn
  | MessageSaved
  | PushedCustomMessage

type SuccessToSingIn = {
  type: 'singin-success'
}
type FailedToSingIn = {
  type: 'singin-failed'
  reason: 'expired token.' | 'no token found.'
}
type MessageSaved = {
  type: 'message'
  message: Message
}
type PushedCustomMessage = {
  type: 'custom'
  speaker: Speaker | null = null
  payload: any
}
```

---

####  テーブル

```ts
// speaker は json | null なカラムで実装
type Speaker = {
  name: string
  id?: any
  avatar?: string // 画像の URI
}

class Message {
  id: number // auto generated
  text: string
  speaker: Speaker | null = null
  roomName: string
  replyTo: number | null = null // Message.id
  createdAt: Date
  updatedAt: Date
}

class Token {
  code: string // unique primary
  speaker: Speaker | null = null
  roomName: string
  expiredAt: Date
}
```