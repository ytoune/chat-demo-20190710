import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	Index,
	CreateDateColumn,
	UpdateDateColumn,
} from 'typeorm'
import { Speaker } from './Speaker'

@Entity('messages')
export class Message {
	@PrimaryGeneratedColumn({ type: 'int', unsigned: true })
	readonly id?: number

	@Column({ type: 'text' })
	text!: string

	@Index()
	@Column({ type: 'varchar', length: 180 })
	roomName!: string

	@Column({ type: 'json', default: null })
	speaker: Speaker | null = null

	@Column({ type: 'int', unsigned: true, default: null })
	replyTo: number | null = null

	@CreateDateColumn()
	createdAt?: Date

	@UpdateDateColumn()
	updatedAt?: Date
}
