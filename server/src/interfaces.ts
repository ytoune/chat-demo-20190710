export interface Observer<T, E = any> {
	next(val: T): void
	error(err: E): void
	complete(): void
}

export type Keytype = string | number | Date | undefined

export interface Repo<T, P extends Keytype> {
	select(): RepoQuery<T>
	find(value: P): Promise<T | null>
	destory(value: P): Promise<boolean>
	save(item: Partial<T>): Promise<boolean>
	watch(observer: Partial<Observer<T>>): () => void
}

type Ops = '=' | '>' | '<'

export interface RepoQuery<T> {
	where<K extends keyof T>(key: K, op: Ops, value: T[K]): this
	get(): Promise<T[]>
	first(): Promise<T | null>
}
