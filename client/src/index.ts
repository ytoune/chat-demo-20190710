import React from 'react'
import { render } from 'react-dom'

import { App } from './comps/App'

const el = document.querySelector('main')
if (el) {
	render(React.createElement(App, null), el)
}
