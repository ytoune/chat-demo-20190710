//@ts-check
const path = require('path')
const { promisify } = require('util')
const writeFile = promisify(require('fs').writeFile)
const { publish } = require('gh-pages')

/** @param {string} filepath */
const main = async filepath => {
	if ('string' !== typeof filepath) return console.log('error')

	filepath = path.resolve(filepath) + '/package.json'

	if ('package.json' !== path.basename(filepath)) return console.log('error')

	const mod = (() => {
		try {
			return require(filepath)
		} catch {}
	})()

	if (!mod) return console.log('not found')

	/** @param {string[]} s */
	const resolve = (...s) => path.join(path.dirname(filepath), ...s)

	const {
		version,
		author,
		license,
		private: private_,
		dependencies,
		main,
		description = '',
	} = mod

	const name = path.basename(path.dirname(filepath))

	const packagerow = JSON.stringify(
		{
			name: 'zono-simple-chat-' + name,
			description,
			version,
			dependencies,
			main,
			author,
			private: private_,
			license,
		},
		null,
		2,
	)

	await writeFile(resolve('output', 'package.json'), packagerow, 'utf8')

	await new Promise((res, rej) => publish(resolve('output'), {
		user: {
			email: 'tone@re-knock.io',
			name: 'publisher',
		},
		branch: `release-${name}-v${version}`,
		dotfiles: true,
		message: `Release ${name} v${version}!!`,
	}, e => e ? rej(e) : res()))

	console.log('done!')
}

main(process.argv[2]).catch(x => {
	console.error(x)
	process.exit()
})
