import * as WebSocket from 'ws'

import { repos, Message, Speaker } from '../db'
import { simpleRepo } from '../utils/simpleRepo'
import { parseJSON } from '../utils/parseJSON'
import { compareAsc } from 'date-fns'
import { Subscription } from 'rxjs'

type Conn = {
	context?: {
		speaker: Speaker
		roomName: string
	}
	end?: () => void
	ws: WebSocket
}

type SendableMessage =
	| SuccessToSingIn
	| FailedToSingIn
	| MessageSaved
	| CustomMessage

type SuccessToSingIn = {
	type: 'singin-success'
}
type FailedToSingIn = {
	type: 'singin-failed'
	reason: string
}
type MessageSaved = {
	type: 'message'
	message: Message
}
type CustomMessage = {
	type: 'custom'
	speaker: Speaker
	payload: any
}

const customDatas = simpleRepo<{
	roomName: string
	speaker: Speaker
	payload: any
}>()

export const onConnect = (ws: WebSocket) => {
	const conn: Conn = { ws }
	const send = (payload: SendableMessage) => ws.send(JSON.stringify(payload))
	ws.on('close', () => {
		if (conn.end) conn.end()
	})
	ws.on('message', data => {
		if ('string' === typeof data) {
			const mes = parseJSON(data)
			if (!mes) return
			switch (mes.type) {
				case 'signin':
					if ('string' === typeof mes.token) {
						const code = mes.token
						repos.tokens.find(code).then(token => {
							if (!token)
								return send({
									type: 'singin-failed',
									reason: 'no token found.',
								})
							if (compareAsc(token.expiredAt, new Date()) < 0)
								return send({
									type: 'singin-failed',
									reason: 'expired token.',
								})
							conn.context = {
								speaker: token.speaker,
								roomName: token.roomName,
							}
							const {
								speaker: { name },
								roomName,
							} = conn.context
							repos.messages.save({
								text: name + 'さんが参加しました。',
								roomName,
							})
							send({ type: 'singin-success' })
							const subscribe = new Subscription()
							subscribe.add(
								repos.messages.watch({
									next: message => {
										if (
											conn.context &&
											conn.context.roomName === message.roomName
										)
											send({ type: 'message', message })
									},
								}),
							)
							subscribe.add(
								customDatas.watch({
									next: ({ speaker, roomName, payload }) => {
										if (conn.context && conn.context.roomName === roomName)
											send({ type: 'custom', speaker, payload })
									},
								}),
							)
							conn.end = () => {
								subscribe.unsubscribe()
								repos.messages.save({
									text: name + 'さんが離席しました。',
									roomName,
								})
							}
						})
					}
					break
				case 'speak':
					if (conn.context && 'string' === typeof mes.text && mes.text) {
						const { speaker, roomName } = conn.context
						const text = mes.text
						repos.messages.save({ speaker, roomName, text })
					}
					break
				case 'custom':
					if (conn.context) {
						const { speaker, roomName } = conn.context
						const payload = mes.payload
						customDatas.save({ speaker, roomName, payload })
					}
					break
			}
		}
	})
}
