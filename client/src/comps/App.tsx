import React from 'react'

import { Form } from './Form'
import { LoginForm } from './LoginForm'
import { useStore } from '../useStore'

export const App = () => {
	const { isLogined } = useStore()
	return <div>{isLogined ? <Form /> : <LoginForm />}</div>
}
