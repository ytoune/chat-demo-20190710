import React from 'react'
import { useInputControl, useOnEnterKeyDown } from './libs'
import { useStore } from '../useStore'

import {
	Paper,
	TextField,
	List,
	ListItem,
	ListItemText,
	ListItemAvatar,
} from '@material-ui/core'

export const Form = () => {
	const { messages, sendMessage } = useStore()
	const [text, onChange, resetText] = useInputControl('')
	const onKeyDown = useOnEnterKeyDown(() => {
		sendMessage(text)
		resetText()
	}, [text, resetText])
	return (
		<Paper>
			<List>
				{messages.map(({ id, text, speaker }) => {
					return (
						<ListItem key={id}>
							{speaker && (
								<ListItemAvatar>
									<ListItemText secondary={speaker.name} />
								</ListItemAvatar>
							)}
							{speaker ? (
								<ListItemText primary={text} />
							) : (
								<ListItemText secondary={text} />
							)}
						</ListItem>
					)
				})}
			</List>
			<TextField
				value={text}
				onChange={onChange}
				onKeyDown={onKeyDown}
				fullWidth
				placeholder="話す"
			/>
		</Paper>
	)
}
