import { RequestHandler } from 'express'

import { repos } from '../db'

export const removeToken: RequestHandler = (req, res, next) => {
	const code = req.params && req.params.code
	if ('string' !== typeof code) {
		return next()
	}
	return repos.tokens
		.destory(code)
		.then(() => res.status(200).json({ ok: true }))
		.catch(next)
}
