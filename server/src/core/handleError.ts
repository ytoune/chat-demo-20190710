import { ErrorRequestHandler } from 'express'

export const handleError: ErrorRequestHandler = (err, _req, res, _next) => {
	handleErrorWs(err)
	return res.status(500).json({ message: 'something broke!' })
}

export const handleErrorWs = (err: any) => {
	console.error(err)
}
