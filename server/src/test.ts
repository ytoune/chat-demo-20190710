import './prepare'
import { conn } from './db/conn'
import { Message } from './db/Message'
import { In, Connection } from 'typeorm'

const main = async (conn: Connection) => {
	const list = await conn
		.getRepository(Message)
		.find({ where: { speakerName: In(['佐藤']) } })

	console.log(list)

	await conn.close()
}

Promise.resolve(conn)
	.then(main)
	.catch(x => {
		console.log('#  ERROR')
		console.error(x)
	})
